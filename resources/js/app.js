/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');
import Vue from 'vue';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import Routes from '@/js/routes.js';
import App from '@/js/views/App';

import Vuex from 'vuex';
Vue.use(Vuex);
import storeData from '@/js/store';
const store = new Vuex.Store(storeData);





const app=new Vue({
    el:'#app',
    store,
    router: Routes,
    render: h => h(App),
});


export default app;