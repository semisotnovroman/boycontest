import Vue from 'vue';
import VueRouter from 'vue-router';

import Main from '@/js/components/Main';


Vue.use(VueRouter);

const router=new VueRouter({
    mode : 'history',
    routes: [
        {
            path : '/:token',
            name: 'main',
            component: Main
        }
    ]
});



export default router;