import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';


const store = {
    state: {
        participants: [],
        part : 3,
    },
    mutations: {
        setParticipants (state, participants) {
            let res = [];
            let part;
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
                part =2;
                state.part=2;
            }else{
                console.log('deks');
                part=3;
                state.part=3;
            }
            while (participants.length) {
                res.push(participants.splice(0, part));
            }
            state.participants=res;
        },
        setCountLikes(state, payLoad){
            for(let index in state.participants){
                for( let index1 in state.participants[index]){
                    if(state.participants[index][index1].id === payLoad.participant_id){
                        if(payLoad.type === 'Красота'){
                            state.participants[index][index1].krasota_likes_count=payLoad.count;
                        }else if(payLoad.type === 'Обаятельность'){
                            state.participants[index][index1].obayatelnost_likes_count=payLoad.count;
                        }else if(payLoad.type === 'Конгениальность'){
                            state.participants[index][index1].congenialnost_likes_count=payLoad.count;
                        }
                    }
                }
            }
        },
        del(state, payLoad){
            for(let index in state.participants){
                for( let index1 in state.participants[index]){
                    if(state.participants[index][index1].id === payLoad.need_id){
                        if(payLoad.type === 'Красота'){
                            state.participants[index][index1].krasota_likes_count--;
                        }else if(payLoad.type === 'Обаятельность'){
                            state.participants[index][index1].obayatelnost_likes_count--;
                        }else if(payLoad.type === 'Конгениальность'){
                            state.participants[index][index1].congenialnost_likes_count--;
                        }
                    }
                }
            }
        }
    },
    actions: {
        getParticipants ({ commit }) {
            return axios.get('/api/getParticipants').then(response => {
                commit('setParticipants', response.data.participants);
            });
        },
        setLike({commit}, payLoad){
            return axios.post('/api/setLike', payLoad).then(response => {
                if(response.data.state === 'dont_was'){
                    commit('setCountLikes', Object.assign(payLoad, { count : response.data.count }));
                }else if(response.data.state === 'was'){
                    commit('setCountLikes', Object.assign(payLoad, { count : response.data.count }));
                    commit('del', Object.assign(payLoad, { need_id : response.data.participant_id }));
                }
            });
        }
    }
};


export  default store;