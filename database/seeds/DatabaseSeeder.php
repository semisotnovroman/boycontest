<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // ТОКЕНЫ
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $max = strlen($charset);
        for($i=0; $i<50; $i++){
            $randStr='';
            for($j=0;$j<40;$j++){
                $randStr.=$charset[rand(0,$max-1)];
            }
            \App\User::create(['token' => $randStr]);

        }


        $participants=[
            'Максим Антонов' => 'maks.jpg',
            'Михаил Кузьмин' => 'misha.jpg',
            'Тигран Мурадян' => 'tigran.jpg',
            'Сослан Зангиев' => 'soslan.jpg',
            'Александр Белавин' => 'balavin.jpg',
            'Андрей Грахов' => 'grahov.jpg',
            'Антон Ждановских' => 'jdanovsky.jpg',
            'Вадим Шахматов' => 'shaxmatov.jpg',
            'Владислав Жуков' => 'jykov.jpg',
            'Вячеслав Сучков' => 'sychkov.jpg',
            'Семисотнов Роман' => 'semisotnov.jpg',
            'Георгий Дробященко' => 'drpb.jpg',
            'Даниил Криванков' => 'krav.jpg',
            'Дмитрий Метальников' => 'metal.jpg',
            'Евгений Емельянов' => 'emel.jpg',
            'Егор Девяткин' => 'dev.jpg',
            'Иван Мельников' => 'mel.jpg',
            'Иван Якушкин' => 'yakush.jpg',
            'Кирилл Папирный' => 'pap.jpg',
            'Лев Овчинников' => 'ovch.jpg',
            'Макар Арапханов' => 'arap.jpg',
        ];

        foreach($participants as $k => $v){
            \App\Participant::create([
                'name' => $k,
                'path' => $v
            ]);
        }









    }


}
