<?php

namespace App\Http\Controllers;

use App\Likes;
use App\User;
use Illuminate\Http\Request;

class LikeController extends Controller
{



    public function create(Request $request){
        $user=User::whereToken($request->header('token'))->first();
        $type=$request->input('type');
        $participant_id=$request->input('participant_id');

        $like=Likes::whereUser_idAndType($user->id, $type)->first();
        if($like !== null){

            if($like->participant_id === $participant_id){
                $like->delete();
                return [
                    'state' => 'dont_was',
                    'count' => Likes::where([
                        'participant_id' => $participant_id,
                        'type' => $type
                    ])->count()
                ];


            }else{
                $response=[
                    'state' => 'was',
                    'participant_id' => $like->participant_id
                ];
                $like->delete();
            }


        }

        Likes::create([
            'user_id' => $user->id,
            'type' => $type,
            'participant_id' => $participant_id
        ]);

        if(isset($response)){
            $response['count']=Likes::where([
                'participant_id' => $participant_id,
                'type' => $type
            ])->count();
            return $response;
        }

        return [
            'state' => 'dont_was',
            'count' => Likes::where([
                'participant_id' => $participant_id,
                'type' => $type
            ])->count()
        ];
    }








}
