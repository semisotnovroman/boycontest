<?php

namespace App\Http\Controllers;


use App\Likes;
use App\Participant;
use App\User;
use Illuminate\Http\Request;


class ParticipantsController extends Controller
{




    public function get(Request $request){
        $participants=Participant::
        withCount('krasota_likes')
            ->withCount('obayatelnost_likes')
            ->withCount('congenialnost_likes')
            ->get();

        $user=User::whereToken($request->header('token'))->first();

        $krasotaCount=Likes::whereUser_idAndType($user->id, 'Красота')->count();
        $oboyatelnostCount=Likes::whereUser_idAndType($user->id, 'Обаятельность')->count();
        $kongenCount=Likes::whereUser_idAndType($user->id, 'Конгениальность')->count();

        return [
            'participants' => $participants,
            'krasotaCount' => $krasotaCount,
            'oboyatelnostCount' => $oboyatelnostCount,
            'kongenCount' => $kongenCount
        ];



    }



}
