<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class checkToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( User::whereToken($request->header('token'))->first() === null){
            return abort(400);
        }
        return $next($request);
    }
}
