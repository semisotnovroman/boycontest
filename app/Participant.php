<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{



    protected $fillable = [
        'name', 'path'
    ];




    public function krasota_likes()
    {
        return $this->hasMany(Likes::class)->whereType('Красота');
    }

    public function obayatelnost_likes()
    {
        return $this->hasMany(Likes::class)->whereType('Обаятельность');
    }

    public function congenialnost_likes()
    {
        return $this->hasMany(Likes::class)->whereType('Конгениальность');
    }






}
